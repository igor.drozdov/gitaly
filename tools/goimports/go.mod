module gitlab.com/gitlab-org/gitaly/tools/goimports

go 1.17

require golang.org/x/tools v0.9.1

require (
	golang.org/x/mod v0.10.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)
